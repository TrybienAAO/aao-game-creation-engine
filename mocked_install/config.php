<?php
/*
Ace Attorney Online - Configuration file
Test install version
*/

function getCfg($str, $language='PHP')
{
	static $conf = array(
		'site_name' => "Ace Attorney Online",
		
		//Resource directories
		'picture_dir' => "../test_install/resources/images/",
			'icon_subdir' => "chars/",
			'talking_subdir' => "chars/",
			'still_subdir' => "charsStill/",
			'startup_subdir' => "charsStartup/",
			'evidence_subdir' => "evidence/",
			'bg_subdir' => "backgrounds/",
			'defaultplaces_subdir' => "defaultplaces/",
			'popups_subdir' => "popups/",
			'locks_subdir' => "psycheLocks/",
		'music_dir' => "../test_install/resources/music/",
		'sounds_dir' => "../test_install/resources/sounds/",
		'voices_dir' => "../test_install/resources/voices/",
		'trialdata_dir' => "../mocked_install/trial_data/",
		'trialdata_backups_dir' => "../mocked_install/trial_data/backups/",
		'trialdata_deleted_dir' => "../mocked_install/trial_data/deleted/",
		'cache_dir' => "../mocked_install/cache/",
		'js_dir' => "Javascript/",
		'css_dir' => "CSS/",
		'lang_dir' => "Languages/",
		
		// Game admins
		'admins' => array(
			'all' => array(1 /* Anonymous */, 2 /*TestAuthor*/),
			'fr' => array(),
			'en' => array(),
			'es' => array(),
			'zh' => array(),
			'de' => array()
		),
		
		// News forums categories
		'news_cat' => array(
			'fr' => 0,
			'en' => 2,
			'es' => 0,
			'de' => 0
		),
		
		// Backup and archive management
		'backups_auto_max_num' => 5,
		'backups_auto_min_delay' => 300,
		'backups_man_max_num' => 5,
		'archives_min_size' => 16384,

		//Link to forum install
		'forum_path' => "../mocked_install/phpbb_files/",
	);
	
	if($str == 'ALL')
	{
		switch($language)
		{
			case 'JS' :
				return json_encode($conf);
				break;
			
			default :
				return $conf;
				break;
		}
	}
	else
	{
		switch($language)
		{
			case 'JS' :
				return json_encode($conf[$str]);
				break;
			
			default :
				return $conf[$str];
				break;
		}
	}
}

?>
