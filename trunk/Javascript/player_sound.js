"use strict";
/*
Ace Attorney Online - Player sound loader

*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'player_sound',
	dependencies : ['trial', 'frame_data', 'soundmanager', 'loading_bar', 'language'],
	init : function()
	{
		if(trial_data)
		{
			// If there is data to preload...
			
			// Set preload object
			var loading_screen = document.getElementById('screen-loading');
			var sounds_loading_label = document.createElement('p');
			sounds_loading_label.setAttribute('data-locale-content', 'loading_sounds');
			loading_screen.appendChild(sounds_loading_label);
			var sounds_loading = new LoadingBar();
			loading_screen.appendChild(sounds_loading.element);
			translateNode(sounds_loading_label);
			
			// Load all music files
			for(var i = 1; i < trial_data.music.length; i++)
			{
				sounds_loading.addOne();
				soundManager.createSound({
					id: 'music_' + trial_data.music[i].id,
					url: getMusicUrl(trial_data.music[i]),
					autoLoad: true,
					autoPlay: false,
					onload: function(success) { if(success) { sounds_loading.loadedOne(); } else { sounds_loading.failedOne(); } },
					onfinish: soundManager.play.bind(undefined, 'music_' + trial_data.music[i].id, {
						position: trial_data.music[i].loop_start
					}),
					volume: trial_data.music[i].volume
				});
			}
			
			// Load all sound files
			for(var i = 1; i < trial_data.sounds.length; i++)
			{
				sounds_loading.addOne();
				soundManager.createSound({
					id: 'sound_' + trial_data.sounds[i].id,
					url: getSoundUrl(trial_data.sounds[i]),
					autoLoad: true,
					autoPlay: false,
					onload: function(success) { if(success) { sounds_loading.loadedOne(); } else { sounds_loading.failedOne(); } },
					volume: trial_data.sounds[i].volume
				});
			}
			
			// Load all voices
			for(var i = 1; i <= 3; i++)
			{
				sounds_loading.addOne();
				soundManager.createSound({
					id: 'voice_-' + i,
					url: getVoiceUrl(-i),
					autoLoad: true,
					autoPlay: false,
					onload: function(success) { if(success) { sounds_loading.loadedOne(); } else { sounds_loading.failedOne(); } },
					loops: 999,
					volume: 70
				});
			}
		}
	}
}));

//INDEPENDENT INSTRUCTIONS
var current_music_id;

//EXPORTED VARIABLES


//EXPORTED FUNCTIONS
function playSound(sound_id)
{
	soundManager.play('sound_' + sound_id);
}

function playMusic(music_id)
{
	if(current_music_id != music_id)
	{
		stopMusic();
		soundManager.play('music_' + music_id);
		current_music_id = music_id;
	}
}

function stopMusic()
{
	soundManager.stop('music_' + current_music_id);
	current_music_id = -1;
}

function createSoundPlayer(url, sound_id)
{
	var player = document.createElement('div');
	addClass(player, 'sound_player');
	
	var play_button = document.createElement('button');
	setNodeTextContents(play_button, '▶');
	player.appendChild(play_button);
	
	var pause_button = document.createElement('button');
	setNodeTextContents(pause_button, '▮▮');
	player.appendChild(pause_button);
	
	var position_bar = document.createElement('progress');
	player.appendChild(position_bar);
	
	var sound = soundManager.getSoundById(sound_id) || soundManager.createSound({
		id: sound_id,
		url: url,
		autoLoad: true,
		autoPlay: false
	});
	
	if(sound.position > 0)
	{
		position_bar.max = sound.durationEstimate;
		position_bar.value = sound.position;
	}
	else
	{
		position_bar.max = 1;
		position_bar.value = 0;
	}
	
	registerEventHandler(play_button, 'click', sound.play.bind(sound, {
		whileplaying: function() {
			position_bar.max = this.durationEstimate;
			position_bar.value = this.position;
		}
	}), false);
	registerEventHandler(pause_button, 'click', sound.pause, false);
	registerEventHandler(position_bar, 'click', function(e) {
		var bar_screen_pos = this.getBoundingClientRect();
		var new_ratio_position = (e.screenX - bar_screen_pos.left) / this.clientWidth;
		var new_position = Math.floor(new_ratio_position * position_bar.max);
		
		sound.play({
			position: new_position,
			whileplaying: function() {
				position_bar.max = this.durationEstimate;
				position_bar.value = this.position;
			}
		});
	}, false);
	
	return player;
}

//END OF MODULE
Modules.complete('player_sound');
