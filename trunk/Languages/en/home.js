{
	"homepage" : "Homepage",
	
	"welcome" : "Welcome to Ace Attorney Online, the online case maker !",
	"site_presentation" : "AAO is the first place to play, create and share adventure games, using gameplay elements from Capcom's Ace Attorney series.",
	"no_download" : "No download, no hassle - just sign up and start writing your own game from your favourite web browser !",
	
	"news" : "Latest news and updates",
	"posted_on" : "Posted on <date> at <time>",
	"view_comments" : "View comments",
	
	"random_featured" : "Featured game",
	
	"affiliates" : "Affiliates",
	"contact" : "Contact"
}
